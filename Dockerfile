FROM rust:1.58.1

WORKDIR /code

RUN cargo install cargo-tarpaulin

ADD . .
