#[cfg(test)]
mod tests {
    use actix_service::Service;
    use actix_web::{test, web, App, Error, HttpResponse};

    async fn my_handler() -> Result<HttpResponse, Error> {
        Ok(HttpResponse::Ok().into())
    }

    async fn my_other_handler() -> Result<HttpResponse, Error> {
        Ok(HttpResponse::Ok().into())
    }

    #[actix_web::test]
    async fn test_example() {
        let app = test::init_service(App::new().service(web::resource("/").to(my_handler))).await;

        let req = test::TestRequest::with_uri("/").to_request();
        let res = app.call(req).await.unwrap();
        assert!(res.status().is_success());
    }
}

fn main() {
    println!("Hello, world!");
}
